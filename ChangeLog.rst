Release 0.2.4 (2014-06-04)
--------------------------

* add Python3 support

Release 0.2.3 (2014-02-22)
--------------------------

* exclude tests in setup.py's find_packages

Release 0.2.2 (2014-02-22)
--------------------------

* include missing LICENSE and version.txt files in MANIFEST.in

Release 0.2.1 (2014-02-21)
--------------------------

* fix typo, use dlr instead of dir
* improve tests

Release 0.2 (2014-02-20)
------------------------

* add support for account balance request

Release 0.1 (2014-02-20)
------------------------

* Initial release
* support for the SMS sending API documented in
  http://www.smstrade.eu/pdf/SMS-Gateway_HTTP_API_v2_en.pdf
